FROM archlinux:latest

LABEL version="0.0.1"
LABEL description="Image to build Arch Linux packages"
LABEL mainteiner="a.a.klevtsov@gmail.com"

ARG PACKAGER="Artem Klevtsov <a.a.klevtsov@gmail.com>"
ARG TAREXT=tar.zst

ENV PKGEXT=.pkg.${TAREXT}
ENV SRCEXT=.src.${TAREXT}
ENV DBEXT=.db.${TAREXT}
ENV PACKAGER=${PACKAGER}
ENV AUR_REPO=custom
ENV AUR_DBROOT=/var/cache/pacman/custom
ENV AUR_BASE_URL=https://aur.archlinux.org
ENV AUR_SNAPHOT_URL=${AUR_BASE_URL}/cgit/aur.git/snapshot

RUN pacman-key --init && \
    pacman --sync --refresh && \
    pacman --sync --needed --noconfirm archlinux-keyring && \
    pacman --sync --needed --noconfirm base-devel ccache rsync subversion git && \
    pacman --sync --sysupgrade --noconfirm && \
    useradd build --system --shell /bin/bash --home-dir /var/cache/build --create-home && \
    echo "build ALL=(root) NOPASSWD: /usr/bin/pacman,/usr/sbin/pacman-key" > /etc/sudoers.d/build && \
    mkdir -p ${AUR_DBROOT} && \
    repo-add  --quiet ${AUR_DBROOT}/${AUR_REPO}${DBEXT} && \
    chown -R build:build ${AUR_DBROOT} && \
    echo "[${AUR_REPO}]" >> /etc/pacman.conf && \
    echo "SigLevel = Optional TrustAll" >> /etc/pacman.conf && \
    echo "Server = file://${AUR_DBROOT}" >> /etc/pacman.conf && \
    sed -i "/^BUILDENV/s/!ccache/ccache/" /etc/makepkg.conf && \
    pacman --sync --refresh && \
    rm -rf /var/cache/pacman/pkg/*

USER build

WORKDIR /var/cache/build

RUN mkdir ~/.gnupg && \
    chmod 700 ~/.gnupg && \
    touch ~/.gnupg/gpg.conf && \
    echo "keyserver hkps://keys.openpgp.org" >> ~/.gnupg/gpg.conf && \
    echo "keyserver-options auto-key-retrieve" >> ~/.gnupg/gpg.conf && \
    echo "keyserver-options no-honor-keyserver-url" >> ~/.gnupg/gpg.conf && \
    gpg --recv-keys 6BC26A17B9B7018A && \
    curl ${AUR_SNAPHOT_URL}/aurutils.tar.gz --output aurutils.tar.gz && \
    tar -x -f aurutils.tar.gz && \
    cd aurutils && \
    makepkg --syncdeps --rmdeps --install --needed --noconfirm && \
    cd .. && \
    rm -f aurutils.tar.gz && \
    rm -rf aurutils && \
    yes | sudo pacman --sync --clean --clean

CMD ["/usr/bin/bash"]
