# AUR CI utils

## Usage

Docker image:

```bash
export IMGNAME=registry.gitlab.com/aur1/utils:latest
export PKGNAME=pandoc-bin
export RUNCMD="aur sync --noview --noconfirm"
docker run --rm -v $PWD/repo:/var/cache/pacman/custom $IMGNAME $RUNCMD $PKGNAME
```

GitLab CI config example:

```yaml
include:
  - project: aur1/utils
    ref: master
    file: /build.yml
  - project: aur1/utils
    ref: master
    file: /sync.yml
  - project: aur1/utils
    ref: master
    file: /repo.yml

pandoc-bin:
  extends: .sync

pages:
  extends: .repo
```
